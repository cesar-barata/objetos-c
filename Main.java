class Pessoa {
    private String nome;
    private int idade;

    public Pessoa(String nome, int idade) {
        this.nome = nome;
        this.idade = idade;
    }

    public void setNome(String novoNome) {
        this.nome = novoNome;
    }

    public void setIdade(int novaIdade) {
        this.idade = novaIdade;
    }

    public String getNome() {
        return this.nome;
    }

    public int getIdade() {
        return this.idade;
    }

    public void falar(Pessoa ouvinte, String mensagem) {
        String nomeFalador = this.getNome();
        String nomeOuvinte = ouvinte.getNome();
        System.out.printf("\n%s fala para %s: %s", nomeFalador, nomeOuvinte, mensagem);
    }
}

public class Main {
    public static void main(String[] args) {
        Pessoa p1 = new Pessoa("Bruce Wayne", 30);
        Pessoa p2 = new Pessoa("Diana Prince", 30);
        System.out.printf("\nDuas pessoas foram criadas: %s e %s", p1.getNome(), p2.getNome());

        p1.setNome("Batman");
        p2.setNome("Mulher Maravilha");

        p1.falar(p2, "Oi!");
        p2.falar(p1, "Oi");
        p1.falar(p2, "Voce vem sempre aqui?");
        p2.falar(p1, "...");
    }
}
