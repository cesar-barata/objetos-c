#include <stdio.h>
#include <stdlib.h>

/**
 * Vamos simular orientação a objetos (básica) em C!
 * Structs farão o papel de classes e funções o papel dos métodos.
 * Tentaremos replicar algumas funcionalidades da orientação a objetos
 * como é feita na linguagem Java.
 */

/**
 * Como não podemos colocar funções dentro de uma struct, para simular
 * métodos (tecnicamente podemos, mas não o faremos para manter as coisas
 * simples) a struct só contém "atributos".
 * A linguagem C não possui modificadores de acesso para atributos
 * (public, protected, private) e tudo é público por padrão.
 */

/**
 * Utilizamos typedef para que C considere "Pessoa" como um tipo de dados
 * e não só como uma mera struct.
 */
typedef struct {
  char* nome; // usamos o tipo char* pois não há tipo String em C
  int idade;
} Pessoa;

/**
 * C não possui o conceito de "construtor", mas podemos simular um
 * utilizando uma função: em Java, quando temos o código
 *
 *            Pessoa p1 = new Pessoa();
 *
 * o operador "new" indica alocação de espaço na memória (neste caso,
 * espaço suficiente para guardar um objeto do tipo "Pessoa") e a chamada
 * ao construtor "Pessoa()" que é responsável por inicializar os atributos.
 * Além disso, a expressão "new Pessoa()" "retorna" o novo objeto Pessoa.
 * Nossa função que simula um construtor deve, portanto, realizar essas
 * tarefas.
 */

/**
 * Em Java, existem tipos primitivos (int, float, double, ...) e tipos
 * referência (Object e seus filhos, como String e tudo que usa "extends").
 * Essa distinção é feita pois quando tipos primitivos são utilizados
 * em chamadas de métodos/funções, cópias dos valores são passadas
 * como parâmetro e/ou retornadas, enquanto para tipos referência,
 * apenas o "endereço de memória" do objeto é copiado em parâmetros e/ou
 * retornos, para evitar cópias de objetos muito grandes!
 * Em C, todos os tipos de dados (int, double, struct, enum, ...)
 * funcionam como tipos primitivos do Java, ou seja, são copiados.
 * Enquanto ponteiros (int*, double*, struct*, enum*, ...) funcionam como
 * os tipos referência do Java.
 */

/**
 * Nosso construtor, chamado "new_Pessoa", retorna um ponteiro (referẽncia)
 * para o novo objeto criado.
 */
Pessoa* new_Pessoa(char* nome, int idade) {
  /**
   * A função "malloc" faz o papel do operador "new", isto é, aloca memória
   * suficiente para guardar um "objeto" da struct Pessoa.
   * Chamamos o ponteiro para o novo objeto de "this" para manter a
   * analogia com Java, mas é uma variável como qualquer outra pois
   * "this" não é palavra reservada em C.
   */
  Pessoa* this = malloc(sizeof (Pessoa));

  // Inicializamos os "atributos" a partir dos parâmetros.
  (*this).nome = nome;
  (*this).idade = idade;

  /**
   * Retornamos a referência ao novo objeto. Isso não é necessário em Java.
   * O "retorno" acontece automaticamente pois o construtor é um
   * "método especial" em Java.
   */
  return this;
}

/**
 * Criamos um método setter para os "atributos" da struct Pessoa.
 * Observe que ambos os métodos recebem um pointeiro (referência) para uma
 * struct pessoa e o chamamos de "this". Fazemos isso para simular o
 * o comportamento em Java, onde "this" refere-se ao objeto cujo código
 * está sendo executado no momento. Em Java, porém, todos os métodos (com
 * exceção dos "static") "recebem" esse parâmetro implicitamente.
 */

void setNome(Pessoa* this, char* novoNome) {
  (*this).nome = novoNome;
}

void setIdade(Pessoa* this, int novaIdade) {
  (*this).idade = novaIdade;
}

/**
 * Uma consequência de receber o parâmetro "this" explicitamente é que
 * o seguinte código em Java:
 *
 *             Pessoa p = new Pessoa("Batman", 10);
 *             p.setNome("Bruce Wayne");
 *             p.setIdade(12);
 *
 * seria escrito em C da seguinte maneira:
 *
 *             Pessoa* p = new_Pessoa("Batman", 10);
 *             setNome(p, "Bruce Wayne");
 *             setIdade(p, 12);
 *
 * (Observe como os métodos/funções "setNome" e "setIdade" sáo chamados
 * em cada caso, em particular, a posição do objeto "p").
 */

/**
 * Escrevemos agora os métodos getter para esses mesmos atributos
 * nos quais também recebemos o parâmetro "this" explicitamente.
 */

char* getNome(Pessoa* this) {
  return (*this).nome;
}

int getIdade(Pessoa* this) {
  return (*this).idade;
}

/**
 * Para comparar novamente, em Java teriamos:
 *
 *              Pessoa p = new Pessoa("Batman", 10);
 *              p.getNome();  // retorna "Batman"
 *              p.getIdade(); // retorna 10
 *
 * Em C:
 *
 *              Pessoa* p = new_Pessoa("Batman", 10);
 *              getNome(p);
 *              getIdade(p);
 */

/**
 * Agora criamos um método/função qualquer para ilustração.
 * Esse método representa uma pessoa falando algo para outra.
 */

void falar(Pessoa* this, Pessoa* ouvinte, char* mensagem) {
  char* nomeFalador = getNome(this);   // this.getNome();
  char* nomeOuvinte = getNome(ouvinte);    // ouvinte.getNome();
  printf("\n%s fala para %s: %s", nomeFalador, nomeOuvinte, mensagem);
}

int main() {
  Pessoa* p1 = new_Pessoa("Bruce Wayne", 30);
  Pessoa* p2 = new_Pessoa("Diana Prince", 30);
  printf("\nDuas pessoas foram criadas: %s e %s", getNome(p1), getNome(p2));

  setNome(p1, "Batman");   // p1.setNome("Batman");
  setNome(p2, "Mulher Maravilha");    // p2.setNome("Mulher Maravilha")

  falar(p1, p2, "Oi!");   // p1.falar(p2, "Oi!");
  falar(p2, p1, "Oi");    // p2.falar(p1, "Oi");
  falar(p1, p2, "Voce vem sempre aqui?");  // p1.falar(p2, "Você vem sempre aqui?");
  falar(p2, p1, "...");   // p2.falar(p1, "...");

  return 0;
}
